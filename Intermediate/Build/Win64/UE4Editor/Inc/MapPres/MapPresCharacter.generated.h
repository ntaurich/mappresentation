// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAPPRES_MapPresCharacter_generated_h
#error "MapPresCharacter.generated.h already included, missing '#pragma once' in MapPresCharacter.h"
#endif
#define MAPPRES_MapPresCharacter_generated_h

#define MapPres_Source_MapPres_MapPresCharacter_h_12_SPARSE_DATA
#define MapPres_Source_MapPres_MapPresCharacter_h_12_RPC_WRAPPERS
#define MapPres_Source_MapPres_MapPresCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MapPres_Source_MapPres_MapPresCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMapPresCharacter(); \
	friend struct Z_Construct_UClass_AMapPresCharacter_Statics; \
public: \
	DECLARE_CLASS(AMapPresCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MapPres"), NO_API) \
	DECLARE_SERIALIZER(AMapPresCharacter)


#define MapPres_Source_MapPres_MapPresCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMapPresCharacter(); \
	friend struct Z_Construct_UClass_AMapPresCharacter_Statics; \
public: \
	DECLARE_CLASS(AMapPresCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MapPres"), NO_API) \
	DECLARE_SERIALIZER(AMapPresCharacter)


#define MapPres_Source_MapPres_MapPresCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMapPresCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMapPresCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMapPresCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapPresCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMapPresCharacter(AMapPresCharacter&&); \
	NO_API AMapPresCharacter(const AMapPresCharacter&); \
public:


#define MapPres_Source_MapPres_MapPresCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMapPresCharacter(AMapPresCharacter&&); \
	NO_API AMapPresCharacter(const AMapPresCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMapPresCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapPresCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMapPresCharacter)


#define MapPres_Source_MapPres_MapPresCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMapPresCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AMapPresCharacter, FollowCamera); }


#define MapPres_Source_MapPres_MapPresCharacter_h_9_PROLOG
#define MapPres_Source_MapPres_MapPresCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MapPres_Source_MapPres_MapPresCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	MapPres_Source_MapPres_MapPresCharacter_h_12_SPARSE_DATA \
	MapPres_Source_MapPres_MapPresCharacter_h_12_RPC_WRAPPERS \
	MapPres_Source_MapPres_MapPresCharacter_h_12_INCLASS \
	MapPres_Source_MapPres_MapPresCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MapPres_Source_MapPres_MapPresCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MapPres_Source_MapPres_MapPresCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	MapPres_Source_MapPres_MapPresCharacter_h_12_SPARSE_DATA \
	MapPres_Source_MapPres_MapPresCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MapPres_Source_MapPres_MapPresCharacter_h_12_INCLASS_NO_PURE_DECLS \
	MapPres_Source_MapPres_MapPresCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAPPRES_API UClass* StaticClass<class AMapPresCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MapPres_Source_MapPres_MapPresCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
