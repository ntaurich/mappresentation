// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAPPRES_MapPresGameMode_generated_h
#error "MapPresGameMode.generated.h already included, missing '#pragma once' in MapPresGameMode.h"
#endif
#define MAPPRES_MapPresGameMode_generated_h

#define MapPres_Source_MapPres_MapPresGameMode_h_12_SPARSE_DATA
#define MapPres_Source_MapPres_MapPresGameMode_h_12_RPC_WRAPPERS
#define MapPres_Source_MapPres_MapPresGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MapPres_Source_MapPres_MapPresGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMapPresGameMode(); \
	friend struct Z_Construct_UClass_AMapPresGameMode_Statics; \
public: \
	DECLARE_CLASS(AMapPresGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MapPres"), MAPPRES_API) \
	DECLARE_SERIALIZER(AMapPresGameMode)


#define MapPres_Source_MapPres_MapPresGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMapPresGameMode(); \
	friend struct Z_Construct_UClass_AMapPresGameMode_Statics; \
public: \
	DECLARE_CLASS(AMapPresGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MapPres"), MAPPRES_API) \
	DECLARE_SERIALIZER(AMapPresGameMode)


#define MapPres_Source_MapPres_MapPresGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MAPPRES_API AMapPresGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMapPresGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MAPPRES_API, AMapPresGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapPresGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MAPPRES_API AMapPresGameMode(AMapPresGameMode&&); \
	MAPPRES_API AMapPresGameMode(const AMapPresGameMode&); \
public:


#define MapPres_Source_MapPres_MapPresGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MAPPRES_API AMapPresGameMode(AMapPresGameMode&&); \
	MAPPRES_API AMapPresGameMode(const AMapPresGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MAPPRES_API, AMapPresGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapPresGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMapPresGameMode)


#define MapPres_Source_MapPres_MapPresGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define MapPres_Source_MapPres_MapPresGameMode_h_9_PROLOG
#define MapPres_Source_MapPres_MapPresGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MapPres_Source_MapPres_MapPresGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MapPres_Source_MapPres_MapPresGameMode_h_12_SPARSE_DATA \
	MapPres_Source_MapPres_MapPresGameMode_h_12_RPC_WRAPPERS \
	MapPres_Source_MapPres_MapPresGameMode_h_12_INCLASS \
	MapPres_Source_MapPres_MapPresGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MapPres_Source_MapPres_MapPresGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MapPres_Source_MapPres_MapPresGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MapPres_Source_MapPres_MapPresGameMode_h_12_SPARSE_DATA \
	MapPres_Source_MapPres_MapPresGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MapPres_Source_MapPres_MapPresGameMode_h_12_INCLASS_NO_PURE_DECLS \
	MapPres_Source_MapPres_MapPresGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MAPPRES_API UClass* StaticClass<class AMapPresGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MapPres_Source_MapPres_MapPresGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
