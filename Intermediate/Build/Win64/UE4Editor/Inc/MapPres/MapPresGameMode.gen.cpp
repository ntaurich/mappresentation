// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MapPres/MapPresGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMapPresGameMode() {}
// Cross Module References
	MAPPRES_API UClass* Z_Construct_UClass_AMapPresGameMode_NoRegister();
	MAPPRES_API UClass* Z_Construct_UClass_AMapPresGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_MapPres();
// End Cross Module References
	void AMapPresGameMode::StaticRegisterNativesAMapPresGameMode()
	{
	}
	UClass* Z_Construct_UClass_AMapPresGameMode_NoRegister()
	{
		return AMapPresGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMapPresGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMapPresGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MapPres,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMapPresGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MapPresGameMode.h" },
		{ "ModuleRelativePath", "MapPresGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMapPresGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMapPresGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMapPresGameMode_Statics::ClassParams = {
		&AMapPresGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMapPresGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMapPresGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMapPresGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMapPresGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMapPresGameMode, 2477966871);
	template<> MAPPRES_API UClass* StaticClass<AMapPresGameMode>()
	{
		return AMapPresGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMapPresGameMode(Z_Construct_UClass_AMapPresGameMode, &AMapPresGameMode::StaticClass, TEXT("/Script/MapPres"), TEXT("AMapPresGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMapPresGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
