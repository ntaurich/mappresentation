// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MapPresGameMode.generated.h"

UCLASS(minimalapi)
class AMapPresGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMapPresGameMode();
};



